﻿using UnityEngine;
using System.Collections;

public class EnemyWitch : Enemy {

	private bool attackMode = false;
	protected new void Start () {
		base.Start ();
	}
	// Use this for initialization
	public override void MoveEnemy (){
		if (attackMode == false)
			return;

		base.MoveEnemy ();
		SoundManager.instance.PlaySingleSource2 (attackSound2);

		//Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
		//AttemptMove <Player> (xDir, yDir);
	}
	public override void takeDamage(int loss){
		print (loss);
		//Subtract lost food points from the enemies total.
		EnemyHP -= loss;
	
		attackMode = true;
		animator.SetTrigger ("AttackMode");
		if (EnemyHP <= 0) {
			
			GameManager.instance.addScore(this.killPoint);
			this.StopAllCoroutines ();
			GameManager.instance.KillEnemy(this);
			dropItem ();
			Destroy (this);
			Destroy (gameObject);
		}
	}

}
