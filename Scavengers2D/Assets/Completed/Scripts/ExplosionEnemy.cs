﻿using UnityEngine;
using System.Collections;

public class ExplosionEnemy : Enemy {

	private int shouldMove;
	public Transform explosionPrefab;
	public AudioClip explosionSound;

	// Use this for initialization
	protected new void Start () {
		base.Start ();
	}

	public override void takeDamage(int loss){
		print (loss);
		//Subtract lost food points from the enemies total.
		EnemyHP -= loss;

		//Check to see if game has ended.
		if (EnemyHP <= 0) {
			explode ();

		}

	}
	private void explode(){
		Instantiate (explosionPrefab, transform.position, Quaternion.identity);
		this.StopAllCoroutines ();
		SoundManager.instance.PlaySingle (explosionSound);
		GameManager.instance.KillEnemy(this);
		dropItem ();
		Destroy (this);
		Destroy (gameObject);
	}

	protected override void OnCantMove <T> (T component)
	{

		//Declare hitPlayer and set it to equal the encountered component.
		Player hitPlayer = component as Player;

		//Call the LoseFood function of hitPlayer passing it playerDamage, the amount of foodpoints to be subtracted.
		hitPlayer.LoseHP (playerDamage);

		//Set the attack trigger of animator to trigger Enemy attack animation.
		base.animator.SetTrigger ("enemyAttack");

		//Call the RandomizeSfx function of SoundManager passing in the two audio clips to choose randomly between.
		SoundManager.instance.RandomizeSfx (attackSound1, attackSound2);
	}

}
