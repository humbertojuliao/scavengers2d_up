﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour 
{
	public AudioSource efxSource;					//Drag a reference to the audio source which will play the sound effects.
	public AudioSource musicSource;					//Drag a reference to the audio source which will play the music.
	public AudioSource efxSource2;

	public static SoundManager instance = null;		//Allows other scripts to call functions from SoundManager.				
	public float lowPitchRange = .95f;				//The lowest a sound effect will be randomly pitched.
	public float highPitchRange = 1.05f;			//The highest a sound effect will be randomly pitched.

	public Slider volumeSlider;
	public Slider sfxSlider;

	
	
	void Awake ()
	{
		//Check if there is already an instance of SoundManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
			Destroy (gameObject);
		
		musicSource.volume = PlayerPrefs.GetFloat ("MusicVol");
		efxSource.volume = PlayerPrefs.GetFloat ("SFXVol");
		efxSource2.volume = PlayerPrefs.GetFloat ("SFXVol");


		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}
	
	
	//Used to play single sound clips.
	public void PlaySingle(AudioClip clip)
	{
		if (!efxSource.isPlaying) {
			//Set the clip of our efxSource audio source to the clip passed in as a parameter.
			efxSource.clip = clip;
		
			//Play the clip.
			efxSource.Play ();
		}
	}
	public void PlaySingleSource2(AudioClip clip)
	{
		//Set the clip of our efxSource audio source to the clip passed in as a parameter.
		efxSource2.clip = clip;

		//Play the clip.
		efxSource2.Play ();

	}

	
	//RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
	public void RandomizeSfx (params AudioClip[] clips)
	{
		//Generate a random number between 0 and the length of our array of clips passed in.
		int randomIndex = Random.Range(0, clips.Length);
		
		//Choose a random pitch to play back our clip at between our high and low pitch ranges.
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);
		
		//Set the pitch of the audio source to the randomly chosen pitch.
		efxSource.pitch = randomPitch;
		
		//Set the clip to the clip at our randomly chosen index.
		efxSource.clip = clips[randomIndex];
		
		//Play the clip.
		efxSource.Play();
	}
	public void ChangeMusicVol(){
		musicSource.volume = volumeSlider.value;

	}
	public void ChangeSFXVol(){
		efxSource.volume = sfxSlider.value;
		efxSource2.volume = sfxSlider.value;
	}
	public void destroyManager(){
		Destroy (this.gameObject);
	}
	public void openOption(){
		this.sfxSlider = GameObject.Find ("SFXSlider").GetComponent<Slider> ();
		this.volumeSlider = GameObject.Find ("MusicSlider").GetComponent<Slider> ();

		this.sfxSlider.value = SoundManager.instance.efxSource.volume;
		this.volumeSlider.value = SoundManager.instance.musicSource.volume;

		this.volumeSlider.onValueChanged.AddListener (delegate {
			this.ChangeMusicVol ();
		});
		this.sfxSlider.onValueChanged.AddListener (delegate {
			this.ChangeSFXVol ();
		});
	}
}
