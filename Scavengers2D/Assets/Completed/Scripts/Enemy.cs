﻿using UnityEngine;
using System.Collections;

//Enemy inherits from MovingObject, our base class for objects that can move, Player also inherits from this.
public class Enemy : MovingObject
{
	public int playerDamage; 							//The amount of food points to subtract from the player when attacking.
	public AudioClip attackSound1;						//First of two audio clips to play when attacking the player.
	public AudioClip attackSound2;						//Second of two audio clips to play when attacking the player.
	
	
	protected Animator animator;							//Variable of type Animator to store a reference to the enemy's Animator component.
	protected Transform target;							//Transform to attempt to move toward each turn.
	protected int moveInTurn = 0;								//Boolean to determine whether or not enemy should skip a turn or move this turn.
	public int movementTurn;
	public int killPoint;
	public bool isBoss;

	public int EnemyHP;

	public GameObject[] drops;

	
	//Start overrides the virtual Start function of the base class.
	protected override void Start ()
	{
		Debug.Log("Entrou Enemy");
		//Register this enemy with our instance of GameManager by adding it to a list of Enemy objects. 
		//This allows the GameManager to issue movement commands.
		GameManager.instance.AddEnemyToList (this);

		Debug.Log("Add Enemy to list");
		
		//Get and store a reference to the attached Animator component.
		animator = GetComponent<Animator> ();

		Debug.Log("Animator");

		StopParticle();

		Debug.Log("Partículas");

		//Find the Player GameObject using it's tag and store a reference to its transform component.
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		//Call the start function of our base class MovingObject.
		base.Start ();
	}
	
	
	//Override the AttemptMove function of MovingObject to include functionality needed for Enemy to skip turns.
	//See comments in MovingObject for more on how base AttemptMove function works.
	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		//Check if skipMove is true, if so set it to false and skip this turn.
		if(moveInTurn != movementTurn)
		{
			moveInTurn++;
			return;
		}
		
		//Call the AttemptMove function from MovingObject.
		base.AttemptMove <T> (xDir, yDir);
		
		//Now that Enemy has moved, set skipMove to true to skip next move.
		moveInTurn = 0;
	}
	
	
	//MoveEnemy is called by the GameManger each turn to tell each Enemy to try to move towards the player.
	public virtual void MoveEnemy ()
	{
		//Declare variables for X and Y axis move directions, these range from -1 to 1.
		//These values allow us to choose between the cardinal directions: up, down, left and right.
		int xDir = 0;
		int yDir = 0;
		
		//If the difference in positions is approximately zero (Epsilon) do the following:
		if(Mathf.Abs (target.position.x - transform.position.x) < Mathf.Epsilon)
			
			//If the y coordinate of the target's (player) position is greater than the y coordinate of this enemy's position set y direction 1 (to move up). If not, set it to -1 (to move down).
			yDir = target.position.y > transform.position.y ? 1 : -1;
		
		//If the difference in positions is not approximately zero (Epsilon) do the following:
		else
			//Check if target x position is greater than enemy's x position, if so set x direction to 1 (move right), if not set to -1 (move left).
			xDir = target.position.x > transform.position.x ? 1 : -1;
		//Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
		AttemptMove <Player> (xDir, yDir);
	}
	
	
	//OnCantMove is called if Enemy attempts to move into a space occupied by a Player, it overrides the OnCantMove function of MovingObject 
	//and takes a generic parameter T which we use to pass in the component we expect to encounter, in this case Player
	protected override void OnCantMove <T> (T component)
	{
		//Declare hitPlayer and set it to equal the encountered component.
		Player hitPlayer = component as Player;
		
		//Call the LoseFood function of hitPlayer passing it playerDamage, the amount of foodpoints to be subtracted.
		hitPlayer.LoseHP (playerDamage);
		
		//Set the attack trigger of animator to trigger Enemy attack animation.
		animator.SetTrigger ("enemyAttack");
		
		//Call the RandomizeSfx function of SoundManager passing in the two audio clips to choose randomly between.
		SoundManager.instance.RandomizeSfx (attackSound1, attackSound2);
	}
	public virtual void takeDamage(int loss){
		//Set the trigger for the player animator to transition to the playerHit animation.
		//animator.SetTrigger ("playerHit");

		//Subtract lost food points from the enemies total.
		EnemyHP -= loss;

		//Check to see if game has ended.
		if (EnemyHP <= 0) {

			if(this.isBoss){
				GameManager.instance.bossBattle = false;
				GameManager.instance.exit();
				SoundManager.instance.PlaySingleSource2 (GameManager.instance.bossComplete);
			}

			GameManager.instance.addScore(this.killPoint);
			this.StopAllCoroutines ();
			GameManager.instance.KillEnemy(this);
			dropItem ();
			Destroy (this);
			Destroy (gameObject);
		}

	}
	public virtual void dropItem(){
		int rolling = Random.Range (1, 100);
		print (rolling);
		Vector3 position = new Vector3((int) this.transform.position.x,(int) this.transform.position.y);
		if (rolling <= 49) {
			Instantiate (drops [2],position, Quaternion.identity);
			return;
		}
		else if(rolling >= 50 && rolling <=89){
			//insira aqui drop de comida
			Instantiate(drops[Random.Range(0,1)],position, Quaternion.identity);
		}
		else if(rolling >= 90 && rolling <= 95){
			//insira aqui drop de arma
			GameObject obj = drops[Random.Range(1,(drops.Length-1))];
			Instantiate(obj,position, Quaternion.identity);

			if(obj.tag == "MeleeWeapon" || obj.tag == "RangedWeapon")
				GameManager.instance.addWeaponToBoard(obj);

		}else{
			//O último drop da lista de drops sempre será o mais raro, com 5% de chances de ser encontrado.
			GameObject obj = drops[drops.Length-1];
			Instantiate(obj,position, Quaternion.identity);

			if(obj.tag == "MeleeWeapon" || obj.tag == "RangedWeapon")
				GameManager.instance.addWeaponToBoard(obj);		
		}
	}
	public void summonParticles(){
		this.StartParticle();
		Invoke("StopParticle", 0.5f);
	}

	protected void StartParticle(){
		if (this.GetComponent<ParticleSystem>() != null){
			ParticleSystem.EmissionModule a = this.GetComponent<ParticleSystem>().emission;
			a.enabled = true;
		}
	}
	protected void StopParticle(){
		if (this.GetComponent<ParticleSystem>() != null){
			ParticleSystem.EmissionModule a = this.GetComponent<ParticleSystem>().emission;
			a.enabled = false;
		}
	}
}

