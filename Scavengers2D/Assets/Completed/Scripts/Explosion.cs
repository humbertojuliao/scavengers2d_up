﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	
	//private BoxCollider2D boxCollider;
	private Animator anim;

	protected void Start () {
		//boxCollider = GetComponent <BoxCollider2D> ();
		anim = GetComponent<Animator> ();

	}
	void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Player")
		{
			coll.gameObject.SendMessage("LoseHP", 50);
			//boxCollider.enabled = false;
		}
		if (coll.gameObject.tag == "Enemy") {
			coll.gameObject.SendMessage ("takeDamage", 50);

		}
	}
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);


		// Code to execute after the delay
	}
	void Update(){
		if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Done")) {
			// Avoid any reload.
			this.gameObject.SetActive (false);
			Destroy (this.gameObject);
		}

	}
}
