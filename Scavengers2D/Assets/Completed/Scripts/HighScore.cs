﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {
	public void highScoreValue(){
		this.GetComponent<Text>().text = "" + PlayerPrefs.GetInt("High Score");
	}
	public void lastScoreValue(){
		this.GetComponent<Text>().text = "" + PlayerPrefs.GetInt("Last Score");
	}
	public void musicVolumeValue(){
		if (!PlayerPrefs.HasKey ("MusicVol")) {
			PlayerPrefs.SetFloat ("MusicVol", 1f);
		}
		GameObject.Find("MusicSlider").GetComponent<Slider> ().value = PlayerPrefs.GetFloat ("MusicVol");
	}
	public void SFXVolumeValue(){
		if (!PlayerPrefs.HasKey ("SFXVol")) {
			PlayerPrefs.SetFloat ("SFXVol", 1f);
		}
		GameObject.Find("SFXSlider").GetComponent<Slider> ().value = PlayerPrefs.GetFloat ("SFXVol");
	}
	public void saveMusicVol(){
		PlayerPrefs.SetFloat("MusicVol", GameObject.Find ("MusicSlider").GetComponent<Slider> ().value);
	}
	public void saveSFXVol(){
		PlayerPrefs.SetFloat("SFXVol", GameObject.Find ("SFXSlider").GetComponent<Slider> ().value);
	}
}
