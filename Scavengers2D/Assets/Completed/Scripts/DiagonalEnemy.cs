﻿using UnityEngine;
using System.Collections;

public class DiagonalEnemy : Enemy {

	// Use this for initialization
	public override void MoveEnemy (){
		int xDir = 0;
		int yDir = 0;
		yDir = target.position.y > transform.position.y ? 1 : -1;

		xDir = target.position.x > transform.position.x ? 1 : -1;

		//Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
		AttemptMove <Player> (xDir, yDir);
	}
	protected new void Start () {
		base.Start ();
	}


}
