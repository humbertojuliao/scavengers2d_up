﻿using UnityEngine;
using System.Collections;

public class NecroBoss : Enemy {

	public Enemy[] summons;
	public GameObject[] dropsT2;
	public GameObject[] dropsT3;

	private bool summonTurn = false;
	// Use this for initialization
	protected override void Start () {
		base.EnemyHP = ((GameManager.instance.getCurrentLevel () / 5) * base.EnemyHP);

		base.Start ();

	}
	
	// Update is called once per frame
	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		//Check if skipMove is true, if so set it to false and skip this turn.
		if(base.moveInTurn != movementTurn)
		{
			moveInTurn++;
			return;
		}
		//Call the AttemptMove function from MovingObject.
		base.AttemptMove <T> (xDir, yDir);
		SummonZmbie ();
		//moveInTurn = 0;
	}

	public override void dropItem(){
		int rolling = Random.Range (1, 100);
		print (rolling);
		GameObject[] dropList;
		if(GameManager.instance.getCurrentLevel ()<25){
			dropList=drops;
		}else if(GameManager.instance.getCurrentLevel ()<50){
			dropList=dropsT2;
		}else{
			dropList=dropsT3;
		}
		Vector3 position = new Vector3((int) this.transform.position.x,(int) this.transform.position.y);
		if (rolling <= 49) {
			Instantiate (dropList [0],position, Quaternion.identity);
			return;
		}
		else if(rolling >= 50 && rolling <=89){
			//insira aqui drop de comida
			Instantiate(dropList[Random.Range(0,1)],position, Quaternion.identity);
		}
		else if(rolling >= 90 && rolling <= 95){
			//insira aqui drop de arma
			GameObject obj = dropList[Random.Range(1,(dropList.Length-1))];
			Instantiate(obj,position, Quaternion.identity);

			if(obj.tag == "MeleeWeapon" || obj.tag == "RangedWeapon")
				GameManager.instance.addWeaponToBoard(obj);

		}else{
			//O último drop da lista de drops sempre será o mais raro, com 5% de chances de ser encontrado.

			GameObject obj = drops[drops.Length-1];
			Instantiate(obj,position, Quaternion.identity);

			if(obj.tag == "MeleeWeapon" || obj.tag == "RangedWeapon")
				GameManager.instance.addWeaponToBoard(obj);	
		}

	}

	private void SummonZmbie(){
		if (summonTurn == false) {
			summonTurn = true;
			return;
		}
		else{
			Vector2 summonPos;
			summonPos.x = Random.Range (2, 6);
			summonPos.y = Random.Range (2, 6);
			var hitColliders = Physics2D.OverlapCircleAll(summonPos, 0.5f);
			GameObject[] enemyArray = GameObject.FindGameObjectsWithTag ("Enemy");
			foreach (GameObject enemies in enemyArray) {
				while(hitColliders.Length>0){
					for(int i = 0; i<hitColliders.Length; i++){
						if( hitColliders[i].CompareTag("Enemy") || hitColliders[i].CompareTag("Player") ){
							summonPos.x = Random.Range (2, 6);
							summonPos.y = Random.Range (2, 6);
							hitColliders = Physics2D.OverlapCircleAll(summonPos, 0.5f);
							break;
						}
					}
					break;
				}
			}
			if (GameManager.instance.getNumberEnemies () <= 3) {
				
				Enemy x = (Enemy)Instantiate (summons [Random.Range (0, summons.Length)], summonPos, Quaternion.identity);
				x.summonParticles();
				SoundManager.instance.PlaySingleSource2(attackSound1);
			}
		
			summonTurn = false;
			return;
		}
	}

	public override void MoveEnemy ()
	{
		//Declare variables for X and Y axis move directions, these range from -1 to 1.
		//These values allow us to choose between the cardinal directions: up, down, left and right.
		int xDir = 0;
		int yDir = 0;

		//If the difference in positions is approximately zero (Epsilon) do the following:
		if (Mathf.Abs (target.position.x - transform.position.x) < float.Epsilon && this.transform.position.y != 7
		    && this.transform.position.y != 0 && this.transform.position.x != 0 && this.transform.position.x != 10)

			//If the y coordinate of the target's (player) position is greater than the y coordinate of this enemy's position set y direction 1 (to move up). If not, set it to -1 (to move down).
			yDir = target.position.y > transform.position.y ? -1 : 1;

		//If the difference in positions is not approximately zero (Epsilon) do the following:
		else if (Mathf.Abs (target.position.x - transform.position.x) > float.Epsilon && this.transform.position.y != 7
		         && this.transform.position.y != 0 && this.transform.position.x != 0 && this.transform.position.x != 10)
			//Check if target x position is greater than enemy's x position, if so set x direction to 1 (move right), if not set to -1 (move left).
			xDir = target.position.x > transform.position.x ? -1 : 1;
		else if (this.transform.position.y == 0 && this.transform.position.x != 0 && this.transform.position.x != 10) {
			xDir = target.position.x > transform.position.x ? -1 : 1;
			print ("baixo");
		} else if (this.transform.position.y == 7 && this.transform.position.x != 10 && this.transform.position.x != 0) {
			print ("cima");
			xDir = target.position.x > transform.position.x ? -1 : 1;
		} else if (this.transform.position.x == 0 && this.transform.position.y != 7 && this.transform.position.y != 0) {
			yDir = target.position.y > transform.position.y ? -1 : 1;
			print ("esquerda");
		} else if (this.transform.position.x == 10 && this.transform.position.y != 7 && this.transform.position.y != 0) {
			yDir = target.position.y > transform.position.y ? -1 : 1;
			print ("direita");
		} else if (this.transform.position.x == 0 && this.transform.position.y == 0){
			print ("inf esq");
			if (target.position.y == 0)
				yDir = 1;
			else
				xDir = 1;
		} else if (this.transform.position.x == 10 && this.transform.position.y == 7){
			print ("sup dir");
			if (target.position.y == 10)
				yDir = -1;
			else
				xDir = -1;
		} else if (this.transform.position.x == 0 && this.transform.position.y == 7){
			print ("sup esq");
			if (target.position.x == 0)
				xDir = 1;
			else
				yDir = -1;
		} else if (this.transform.position.x == 10 && this.transform.position.y == 0){
			print ("inf dir");
			if (target.position.x == 10)
				xDir = -1;
			else
				yDir = 1;
		}
		//Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
		AttemptMove <Player> (xDir, yDir);
	}
}
