﻿using UnityEngine;
using System.Collections;

public class WeaponAttack: MonoBehaviour {

	private BoxCollider2D boxCollider;
	public LayerMask blockingLayer;	
	private Rigidbody2D rb2D;
	private Animator anim;

	public Vector2 direction;
	public Vector3 targetPosition;

	private int horizontalSize;
	private int verticalSize;
	private int damage;
	private bool isRanged;
	private float speed;
	private int maxRange;
	float step;

	// Use this for initialization
	protected void Start () {
		boxCollider = GetComponent <BoxCollider2D> ();
		rb2D = GetComponent <Rigidbody2D> ();

		Player scriptPlayer;
		scriptPlayer = GameObject.Find("Player").GetComponent<Player>();

		if (scriptPlayer.ModoRanged == true) {

			GenericWeapon rWep = scriptPlayer.rangedWeapon;
			this.setStatus (rWep.horizontalSize, rWep.verticalSize, rWep.damage, rWep.isRanged, rWep.speed, rWep.maxRange);
			step = speed * Time.deltaTime;

			targetPosition = RangedAttack ();
			SoundManager.instance.PlaySingleSource2 (rWep.attackSound);
		} else if (scriptPlayer.ModoRanged == false) {
			GenericWeapon rWep = scriptPlayer.meleeWeapon;
			this.setStatus (rWep.horizontalSize, rWep.verticalSize, rWep.damage, rWep.isRanged, rWep.speed, rWep.maxRange);
			step = speed * Time.deltaTime;
			targetPosition = RangedAttack ();
			SoundManager.instance.PlaySingleSource2 (rWep.attackSound);

		}

		boxCollider.size = new Vector2 (horizontalSize, verticalSize);
	}
	public void setStatus(int hSize, int vSize, int dmg, bool isRanged, float spd, int maxR){
		this.horizontalSize = hSize;
		this.verticalSize = vSize;
		this.damage = dmg;
		this.isRanged = isRanged;
		this.speed = spd;
		this.maxRange = maxR;
	}
		
	
	public void Attack(){
		//Inserir animação de ataque
		StartCoroutine(ExecuteAfterTime(0.5f));

	}
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);
		this.gameObject.SetActive (false);
		Destroy (this.gameObject);

		// Code to execute after the delay
	}
	void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Enemy")
		{
			coll.gameObject.SendMessage("takeDamage", damage);
		}
	}
	public Vector3 RangedAttack(){
		Player scriptPlayer;
		scriptPlayer = GameObject.Find("Player").GetComponent<Player>();

		Vector3 positi;
		if (isRanged == false) {
			//Attack ();
			positi = transform.position;
		}
		else{
			if (Mathf.Abs (direction.x) > Mathf.Abs (direction.y)) {
				//If x is greater than zero, set horizontal to 1, otherwise set it to -1
				if (direction.x > 0) {
					//DIREITA 
					positi = new Vector3 (scriptPlayer.transform.position.x + this.maxRange , scriptPlayer.transform.position.y);
				} else {
					//ESQUERDA
					positi = new Vector3 (scriptPlayer.transform.position.x - this.maxRange, scriptPlayer.transform.position.y);

				}
			}
			else{
				if (direction.y > 0) {
					positi = new Vector3 (scriptPlayer.transform.position.x, scriptPlayer.transform.position.y + this.maxRange);
				} else {
					positi = new Vector3 (scriptPlayer.transform.position.x, scriptPlayer.transform.position.y - this.maxRange);
				}

			}
		}

		StartCoroutine(ExecuteAfterTime(1f));
		return positi;

	}
	void Update() {
		transform.position = Vector3.MoveTowards (this.transform.position, targetPosition, step);
		if ((transform.position == targetPosition) && isRanged == true)
			Destroy (this.gameObject);
	}

	public void RotateShot(int h, int v){
		//		Rotação do disparo, segundo os valores de entrada.( Valores possíveis: -1,0,1 )
		if(h==1 && v==0){
			this.transform.Rotate(Vector3.forward * 0);// = 0
		}else if(h==0 && v==1){
			this.transform.Rotate(Vector3.forward * 90);//= 90
		}else if(h==-1 && v==0){
			this.transform.Rotate(Vector3.forward * 180);//= 180
		}else{
			this.transform.Rotate(Vector3.forward * 270);//= 270
		}
	}
}
