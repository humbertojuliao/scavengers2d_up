﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using System.Collections.Generic;		//Allows us to use Lists. 
using UnityEngine.UI;					//Allows us to use UI.


public class GameManager : MonoBehaviour
{
	
	public float levelStartDelay = 2f;						//Time to wait before starting level, in seconds.
	public float turnDelay = 0.5f;							//Delay between each Player turn.
	public int playerFoodPoints = 100;						//Starting value for Player food points.
	public int playerHitPoints = 100;
	public int maxfood = 100;
	public int maxHP = 100;
	public static GameManager instance = null;				//Static instance of GameManager which allows it to be accessed by any other script.
	[HideInInspector] public bool playersTurn = true;		//Boolean to check if it's players turn, hidden in inspector but public.
	
	
	private Text levelText;									//Text to display current level number.
	private Text regenText;
	private GameObject levelImage;							//Image to block out level as levels are being set up, background for levelText.
	private GameObject buttonChange;
	private GameObject buttonOptions;

	private BoardManager boardScript;						//Store a reference to our BoardManager which will set up the level.
	private int level = 0;									//Current level number, expressed in game as "Day 1".
	private List<Enemy> enemies;							//List of all Enemy units, used to issue them move commands.
	private bool enemiesMoving;								//Boolean to check if enemies are moving.
	public bool doingSetup = true;							//Boolean to check if we're setting up board, prevent Player from moving during setup.
	public bool paused = false;
	public bool bossBattle = false;
	public int score = 0;

	public bool PossoPegarArma = true;
	public int contadorArma = 0;

	public GenericWeapon meleeWeapon;
	public GenericWeapon rangedWeapon;

	private Button trocaArma;
	public bool modoRangedAtivado;

	public AudioClip bossDay;
	public AudioClip regenHP;
	public AudioClip bossComplete;

	public Sprite rangedtoMelee;
	public Sprite meleetoRanged;

	//Extra Functions:
	//Verify if has a Enemy in a passed position
	public void KillEnemy(Enemy killedEm){
		enemies.Remove(killedEm);
	}

	public bool HaveAEnemyHere(Vector3 posi){
		foreach (Enemy em in enemies){
			if(em.transform.position.x == posi.x && em.transform.position.y == posi.y){
				return true;
			}
		}
		return false;
	}
	
	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
			
			//if not, set instance to this
			instance = this;
		
		//If instance already exists and it's not this:
		else if (instance != this )
			
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);	
		
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);

		//Assign enemies to a new List of Enemy objects.
		enemies = new List<Enemy>();

		print (level);
		//Get a component reference to the attached BoardManager script
		boardScript = GetComponent<BoardManager>();
		
		//Call the InitGame function to initialize the first level 
		//LEMBRAR DE ATIVAR E DESATIVAR AQUI PARA DEBUG
		//InitGame();
		print ("shen 2");

	}
	
	//This is called each time a scene is loaded.
	void OnLevelWasLoaded(int index)
	{
		//Add one to our level number.
		level++;
		//Call InitGame to initialize our level.
		InitGame();
		print ("shen 1");


	}
	
	//Initializes the game for each level.
	void InitGame()
	{
		GUI.enabled = false;

		//While doingSetup is true the player can't move, prevent player from moving while title card is up.
		doingSetup = true;



		//Get a reference to our image LevelImage by finding it by name.
		levelImage = GameObject.Find("LevelImage");

		buttonChange = GameObject.Find("ButtonChange");
		Text buttonText = buttonChange.GetComponentInChildren<Text> ();
		Image spriteBut = GameObject.Find ("ChangeWep").GetComponent<Image> ();

		if (modoRangedAtivado == true) {
			buttonText.text = "Ranged";
			spriteBut.sprite = rangedtoMelee;

		} else {
			buttonText.text = "Melee";
			spriteBut.sprite = meleetoRanged;

		}


		buttonChange.SetActive (false);

		buttonOptions = GameObject.Find ("ButtonOptions");
		buttonOptions.SetActive (false);

		//Get a reference to our text LevelText's text component by finding it by name and calling GetComponent.
		levelText = GameObject.Find("LevelText").GetComponent<Text>();
		regenText = GameObject.Find("RegenText").GetComponent<Text>();

		if (level % 5 == 0) {
			levelText.color = Color.red;
			SoundManager.instance.PlaySingleSource2 (bossDay);
		}
		//Set the text of levelText to the string "Day" and append the current level number.
		levelText.text = "Day " + level;
		
		//Set levelImage to active blocking player's view of the game board during setup.
		levelImage.SetActive(true);
		
		//Call the HideLevelImage function with a delay in seconds of levelStartDelay.
		Invoke("HideLevelImage", levelStartDelay);
		
		//Clear any Enemy objects in our List to prepare for next level.
		enemies.Clear();


		RegenHP ();
		if (level == 1)
			regenText.text = "";
		
		//Call the SetupScene function of the BoardManager script, pass it current level number.
		boardScript.SetupScene(level);
		
	}
	
	
	//Hides black image used between levels
	void HideLevelImage()
	{
		//Disable the levelImage gameObject.
		levelImage.SetActive(false);

		buttonOptions.SetActive (true);
		buttonChange.SetActive (true);
		//Set doingSetup to false allowing player to move again.

		doingSetup = false;

	}

	void RegenHP(){
		//Ajustar esses números depois
		if (playerFoodPoints == 0) {
			regenText.text = "+ 0 HP";
			regenText.color = Color.red;
			return;
		}
		else if (playerFoodPoints > 0 && playerFoodPoints <= 20) {
			playerHitPoints += 5;
			regenText.text = "+ 5 HP";
			regenText.color = Color.yellow;
			if (level != 1)
				SoundManager.instance.PlaySingleSource2 (regenHP);

			if (playerHitPoints > maxHP)
				playerHitPoints = maxHP;
		} else if (playerFoodPoints > 20 && playerFoodPoints <= 50) {
			playerHitPoints += 10;
			regenText.text = "+ 10 HP";
			regenText.color = Color.yellow;
			if (level != 1)
				SoundManager.instance.PlaySingleSource2 (regenHP);
			if (playerHitPoints > maxHP)
				playerHitPoints = maxHP;
		} else if (playerFoodPoints > 50 && playerFoodPoints <= 100) {
			playerHitPoints += 25;
			regenText.text = "+ 25 HP";
			regenText.color = Color.green;
			if (level != 1)
				SoundManager.instance.PlaySingleSource2 (regenHP);
			if (playerHitPoints > maxHP)
				playerHitPoints = maxHP;
		}else{
			regenText.text = "+ 30 HP";
			playerHitPoints += 30;
			regenText.color = Color.green;
			if (level != 1)
				SoundManager.instance.PlaySingleSource2 (regenHP);
			if (playerHitPoints > maxHP)
				playerHitPoints = maxHP;
		}
			
	}
	
	//Update is called every frame.
	void Update()
	{
		if (!paused){
			//Check that playersTurn or enemiesMoving or doingSetup are not currently true.
			if(playersTurn || enemiesMoving || doingSetup)
			
				//If any of these are true, return and do not start MoveEnemies.
				return;
		
			//Start moving enemies.
			StartCoroutine (MoveEnemies ());
		}
	}
	
	//Call this to add the passed in Enemy to the List of Enemy objects.
	public void AddEnemyToList(Enemy script)
	{
		//Add Enemy to List enemies.
		enemies.Add(script);
	}
	
	
	//GameOver is called when the player reaches 0 food points
	public void GameOver()
	{
		if(PlayerPrefs.GetInt("High Score")<this.score){
			PlayerPrefs.SetInt("High Score", this.score);
		}
		PlayerPrefs.SetInt("Last Score", this.score);
		//Set levelText to display number of levels passed and game over message
		levelText.text = "After " + level + " days, \n you are dead. \n \n Score:" + score;
		regenText.text = "";

		//Enable black background image gameObject.
		GameObject options = GameObject.Find("ButtonOptions");
		options.SetActive (false); 
		GameObject change = GameObject.Find("ButtonChange");
		change.SetActive (false); 
		SoundManager.instance.destroyManager ();
		levelImage.SetActive(true);
		StartCoroutine(this.NewGameAfterTime(2f));
		//Disable this GameManager.
		enabled = false;
 
	}
	
	//Coroutine to move enemies in sequence.
	IEnumerator MoveEnemies()
	{
		//While enemiesMoving is true player is unable to move.
		enemiesMoving = true;
		
		//Wait for turnDelay seconds, defaults to .1 (100 ms).
		yield return new WaitForSeconds(turnDelay);
		
		//If there are no enemies spawned (IE in first level):
		if (enemies.Count == 0) 
		{
			//Wait for turnDelay seconds between moves, replaces delay caused by enemies moving when there are none.
			yield return new WaitForSeconds(turnDelay);
		}
		
		//Loop through List of Enemy objects.
		for (int i = 0; i < enemies.Count; i++)
		{
			//Call the MoveEnemy function of Enemy at index i in the enemies List.
			if (enemies [i] != null) {
				
				enemies [i].MoveEnemy ();
			}
			//Wait for Enemy's moveTime before moving next Enemy, 
			yield return new WaitForSeconds((enemies[i].moveTime)/enemies.Count);
		}
		//Once Enemies are done moving, set playersTurn to true so player can move.
		playersTurn = true;
		
		//Enemies are done moving, set enemiesMoving to false.
		enemiesMoving = false;
		contadorArma--;
		if (contadorArma <0){
			contadorArma = 0;
		}
		//print ("Contador Arma: " + contadorArma);
		if (contadorArma == 0) {
			PossoPegarArma = true;
		}
	}
	public bool getEnemiesMoving(){
		return enemiesMoving;
	}

	public int getCurrentLevel(){
		return level;
	}

	public virtual void OnPauseGame ()
	{
		paused = true;
	}
	public virtual void OnResumeGame ()
	{
		paused = false;
	}
	public void addScore(int value){
		this.score += value;
		GameObject.Find ("Player").GetComponent<Player> ().scoreText.text = "Score: " + GameManager.instance.score;

	}

	IEnumerator NewGameAfterTime(float time)
	{
		yield return new WaitForSeconds(time);

		SceneManager.LoadScene (0);

		//Application.LoadLevel(0);
		Destroy(GameObject.Find("SoundManager"));
		Destroy(this.gameObject);
		// Code to execute after the delay
	}

	public void addWeaponToBoard(GameObject obj){
		this.boardScript.addWeaponToList(obj);
	}
		
	public int getNumberEnemies(){
		return enemies.Count;
	}
	public void exit(){
		this.boardScript.spawnExit();
	}
}
		


