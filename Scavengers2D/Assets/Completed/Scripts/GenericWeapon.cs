﻿using UnityEngine;
using System.Collections;

public class GenericWeapon : MonoBehaviour {

	public int horizontalSize;
	public int verticalSize;
	public int damage;
	public bool isRanged;
	public float speed;
	public int maxRange;

	public WeaponAttack disparo;
	public AudioClip attackSound;

	// Use this for initialization
	protected virtual void Start () {
	}

	public void runShit(){
		disparo.setStatus (horizontalSize, verticalSize, damage, isRanged, speed, maxRange);
	}
}
