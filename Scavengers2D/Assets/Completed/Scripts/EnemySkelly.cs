﻿using UnityEngine;
using System.Collections;

public class EnemySkelly : Enemy {

	// Use this for initialization
	private bool ModoMorto = false;
	private int turnsToRevive = 5;
	private int ReviveHP = 20;
	protected new void Start () {
		base.Start ();
	}
	public override void MoveEnemy (){
		if (ModoMorto == true) {
			if (turnsToRevive == 0) {
				turnsToRevive = 5;
				ModoMorto = false;
				animator.SetTrigger ("StopMode");
				EnemyHP = ReviveHP;
			}
			turnsToRevive--;	
			return;
		}
		base.MoveEnemy ();

	}
	public override void takeDamage(int loss){
		if (ModoMorto == true)
			return;
		
		print (loss);
		//Subtract lost food points from the enemies total.
		EnemyHP -= loss;
		if (EnemyHP <= 0) {
			GameManager.instance.addScore(this.killPoint);
			this.StopAllCoroutines ();
			ModoMorto = true;
			animator.SetTrigger ("AttackMode");

		}
	}
}
